//
//  Forecast.swift
//  WeatherApp
//
//  Created by Matt Lee on 29/08/2019.
//  Copyright © 2019 AND Digital. All rights reserved.
//

import Foundation

struct Forecast: Codable {
    let list: [ForecastEntry]
}

struct ForecastEntry: Codable {
    
    // MARK: Public
    
    let time: Int
    let precipProbability: Double
    
    var summary : String {
        return weathers.first!.description
    }

    var icon : String {
        return weathers.first!.icon
    }
    
    var temperatureHigh : Double {
        return main.temp_max
    }
    
    init(time: Int, summary: String, icon: String, precipProbability: Double, temperatureHigh: Double) {
        self.time = time
        self.precipProbability = precipProbability
        self.weathers = [Weather(description: summary, icon: icon)]
        self.main = Main(temp_max: temperatureHigh)
    }
    
    // MARK: Private (Parsing JSON structure)
    
    fileprivate let main: Main
    fileprivate let weathers: [Weather]
    
    enum CodingKeys: String, CodingKey {
        case time = "dt"
        case precipProbability = "pop"
        case main = "main"
        case weathers = "weather"
    }
}

fileprivate struct Main: Codable {
    let temp_max: Double
}

fileprivate struct Weather: Codable {
    let description: String
    let icon: String
}
